variable "project_id" {
  description = "project id"
}

variable "region" {
  description = "region"
}

variable "cidr" {
  description = "CIDR block for GKE network"
}