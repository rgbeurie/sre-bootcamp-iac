variable "region" {
  description = "region"
}

variable "project_id" {
  description = "project id"
}

variable "gke_num_nodes" {
  default     = 1
  description = "number of gke nodes"
}

variable "machine_type" {
  description = "Machine flavor"
}

variable "network" {
  description = "VPC name to use for GKE cluster"
}

variable "subnetwork" {
  description = "Subnet name"
}