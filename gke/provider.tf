terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.52.0"
    }
  }

  required_version = ">= 0.14"
}

provider "google" {
  project = var.project_id
  region  = var.region
}

data "google_client_config" "provider" {}

data "google_container_cluster" "bootcamp-gke-cluster" {
  name     = google_container_cluster.primary.name
  location = var.region
}

provider "kubernetes" {
  host  = "https://${data.google_container_cluster.bootcamp-gke-cluster.endpoint}"
  token = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(
    data.google_container_cluster.bootcamp-gke-cluster.master_auth[0].cluster_ca_certificate,
  )
}