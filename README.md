# GKE CLuster with Terraform

This Terraform module is itended to create the first element of the stack  
used in our final project of SRE skill Up Bootcamp
---
## Team Members
- Beurie Reyes
- Nicholas Farias
- José Castrejón
- Ulises Lárraga

### Requirements
- Terraform v0.14.7
- Hashicorp Google provider v3.52.0
- Atlantis
