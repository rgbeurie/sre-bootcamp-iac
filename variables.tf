variable "region" {
  description = "region"
}

variable "project_id" {
  description = "project id"
}

variable "gke_num_nodes" {
  default     = 1
  description = "number of gke nodes"
}

variable "machine_type" {
  description = "Machine flavor"
}

variable "cidr" {
  description = "CIDR block for GKE network"
}
