module "network_resources" {
  source     = "./network"
  project_id = var.project_id
  region     = var.region
  cidr       = var.cidr
}

module "gke_cluster" {
    source       = "./gke"
    project_id   = var.project_id
    region       = var.region
    network      = module.network_resources.network_name
    subnetwork   = module.network_resources.subnetwork_name
    #gke_num_nodes = 1 optional parameter
    machine_type = var.machine_type
} 